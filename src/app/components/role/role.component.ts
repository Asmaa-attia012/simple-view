import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { HttpClientModule , HttpClient} from '@angular/common/http';
import {Role} from '../../models/role';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  roles:Role[]=[];
   constructor( private http:HttpClient , private router:Router) { }

  ngOnInit() {
     //get all role list 
    this.http.get("http://ds147440.mlab.com:3000/roles").subscribe(response=>{
     this.roles=response as Role[];
    },error=>{
      alert("sorry error occurred");
    });
  }

}
