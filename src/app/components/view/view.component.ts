import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { HttpClientModule , HttpClient} from '@angular/common/http';
import {View} from '../../models/view';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})

export class ViewComponent implements OnInit {

  views:View[]=[];
   name:string;
   constructor( private http:HttpClient , private router:Router) { }

    ngOnInit() {
    //get all views list
    this.http.get("http://ds147440.mlab.com:3000/views").subscribe(response=>{
     this.views=response as View[];
    },error=>{
      alert("sorry error occurred");
    });

}}
