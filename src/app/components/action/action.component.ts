 import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { HttpClientModule , HttpClient} from '@angular/common/http';
import { from } from 'rxjs';
import { Action } from '../../models/action';


@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent implements OnInit {
   actions:Action[]=[];
   name:string;
   constructor( private http:HttpClient , private router:Router) { }

    ngOnInit() {
    
    this.http.get("http://ds147440.mlab.com:3000/permissions").subscribe(response=>{
     this.actions=response as Action[];
    },error=>{
      alert("sorry error occurred");
    });

  
  }

}
