
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ActionComponent} from './components/action/action.component';
import {RoleComponent} from './components/role/role.component';
import {ViewComponent} from './components/view/view.component';
import { HttpClient ,HttpClientModule } from '@angular/common/http';

const routes: Routes = [
 
  {
    path:'action',
    component:ActionComponent
  },
  {
    path:'role',
    component:RoleComponent
  },
  {
    path:'view',
    component:ViewComponent
  },
  {
    path:'',
    component:ActionComponent
  },
  {
    path:'**',
    component:ActionComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    HttpClientModule
  ],
  exports: [RouterModule,HttpClientModule]
})
export class AppRoutingModule { }
